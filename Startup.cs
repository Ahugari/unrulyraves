﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UnrulyRave.Startup))]
namespace UnrulyRave
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
